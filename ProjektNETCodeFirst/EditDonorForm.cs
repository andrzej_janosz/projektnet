﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using System.Data.SqlClient;

namespace ProjektNETCodeFirst
{
    public partial class EditDonorForm : Form
    {
        public BloodBankManager bloodBankManager;
        private BloodDonor pickedBloodDonor;
        
        public String newDate;
        public EditDonorForm(BloodBankManager bloodBankManager, BloodDonor bloodDonor)
        {
            InitializeComponent();
            this.bloodBankManager = bloodBankManager;
            this.pickedBloodDonor = bloodDonor;
        }

        private void EditDonorForm_Load(object sender, EventArgs e)
        {
            firstNametextBox.Text = pickedBloodDonor.FirstName;
            lastNametextBox.Text = pickedBloodDonor.LastName;
            dateOfBirthmonthCalendar.SetDate(pickedBloodDonor.DateofBirth);
            bloodTypecomboBox.DataSource = Enum.GetValues(typeof(BloodTypes));
            bloodTypecomboBox.SelectedItem = (BloodTypes)pickedBloodDonor.BloodType;
        }

        private void saveChangesButton_Click(object sender, EventArgs e)
        {
            if((!firstNametextBox.Text.Equals(pickedBloodDonor.FirstName)) || (!lastNametextBox.Text.Equals(pickedBloodDonor.LastName)) ||
                (!System.DateTime.Equals(dateOfBirthmonthCalendar.SelectionEnd, pickedBloodDonor.DateofBirth)) ||(!bloodTypecomboBox.SelectedValue.Equals((BloodTypes)pickedBloodDonor.BloodType)) )
            {

                pickedBloodDonor.FirstName = firstNametextBox.Text;
                pickedBloodDonor.LastName = lastNametextBox.Text;
                pickedBloodDonor.DateofBirth = new DateTime(dateOfBirthmonthCalendar.SelectionEnd.Year, dateOfBirthmonthCalendar.SelectionEnd.Month, dateOfBirthmonthCalendar.SelectionEnd.Day);
                pickedBloodDonor.DateOfRegistration=pickedBloodDonor.DateOfRegistration;
                pickedBloodDonor.BloodType=(BloodTypes)bloodTypecomboBox.SelectedValue;
                
                

                BloodBankManager.context.Entry(pickedBloodDonor).State = System.Data.Entity.EntityState.Modified;
                BloodBankManager.context.SaveChanges();
                bloodBankManager.dataGridView1.DataSource = BloodBankManager.context.BloodDonors.Local.ToBindingList();
                MessageBox.Show("Changes saved");
                this.Close();

                
            }
            else
            {
                MessageBox.Show("No changes were made to the record, quitting");
                this.Close();
            }
        }
    }
}
