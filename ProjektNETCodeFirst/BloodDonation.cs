﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjektNETCodeFirst
{
    public class BloodDonation
    {
        public int BloodDonationId { get; set; }
        public string LastName { get; set; }
        public string BloodType { get; set; }  
        public virtual BloodDonor bloodDonor { get; set; }
        public virtual StoredBlood storedBlood { get; set; }
        public DateTime DateOfDonation { get; set; }
        public String ExtraInformation { get; set; }
        
        
        
    }
}
