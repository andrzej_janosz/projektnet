﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;

namespace ProjektNETCodeFirst
{
    public partial class NewBloodDonationForm : Form
    {
        public String extraInfo;
        public BloodDonor donatingBloodDonor;
        public BloodBankManager bloodBankManager;
        public NewBloodDonationForm(BloodBankManager bloodBankManager)
        {
            InitializeComponent();
            this.bloodBankManager = bloodBankManager;

        }
        

        

        private void NewBloodDonationForm_Load(object sender, EventArgs e)
        {
            donatorcomboBox.DataSource = BloodBankManager.context.BloodDonors.Local.ToBindingList();
            donatorcomboBox.DisplayMember = "LastName";
            donatorcomboBox.ValueMember = "LastName";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var query= from blooddonors in BloodBankManager.context.BloodDonors
                       where blooddonors.LastName.Equals(donatorcomboBox.Text)
                       select blooddonors;
            foreach(var item in query) {
                donatingBloodDonor=item;
            }
            BloodDonation newDonation = new BloodDonation()
            {
                DateOfDonation = DateTime.Now,
                ExtraInformation = textBox1.Text,
                bloodDonor=donatingBloodDonor,
                LastName=donatingBloodDonor.LastName,
                BloodType=donatingBloodDonor.BloodType.ToString(),
                storedBlood = new StoredBlood()
                {
                    bloodType=donatingBloodDonor.BloodType,
                }



            };

            switch (donatingBloodDonor.BloodType)
            {
                case BloodTypes.ABRHplus:
                    {
                        bloodBankManager.amountofabrhplus += 0.45;
                        
                        break;
                    }
                case BloodTypes.ABRHminus:
                    {
                        bloodBankManager.amountofabrhminus += 0.45;
                        
                        break;
                    }
                case BloodTypes.ARHplus:
                    {
                        bloodBankManager.amountofarhplus += 0.45;
                        
                        break;
                    }
                case BloodTypes.ARHminus:
                    {
                        bloodBankManager.amountofarhminus += 0.45;
                        
                        break;
                    }
                case BloodTypes.BRHplus:
                    {
                        bloodBankManager.amountofbrhplus += 0.45;
                        
                        break;
                    }
                case BloodTypes.BRHminus:
                    {
                        bloodBankManager.amountofbrhminus += 0.45;
                        
                        break;
                    }
                case BloodTypes.ORHplus:
                    {
                        bloodBankManager.amountoforhplus += 0.45;
                        
                        break;
                    }
                case BloodTypes.ORHminus:
                    {
                        bloodBankManager.amountoforhminus += 0.45;
                        
                        break;
                    }




            }
            BloodBankManager.context.BloodDonations.Add(newDonation);
            BloodBankManager.context.SaveChanges();

            bloodBankManager.updateAmounOfStoredBlood();


            MessageBox.Show(donatingBloodDonor.FirstName+" "+ donatingBloodDonor.LastName + " donated 450 ml of " + donatingBloodDonor.BloodType.ToString() + " blood.");
            this.Close();
        }
    }
}
