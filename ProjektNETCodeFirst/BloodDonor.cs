﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektNETCodeFirst
{
    public class BloodDonor
    {
        public int BloodDonorId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public DateTime DateofBirth { get; set; }
        public DateTime DateOfRegistration { get; set; }
        public BloodTypes BloodType { get; set; }

        public virtual List<BloodDonation> BloodDonations { get; set; }


    }
}
