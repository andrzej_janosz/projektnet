﻿namespace ProjektNETCodeFirst
{
    partial class DonationsListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.donationsDataGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.donationsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // donationsDataGridView
            // 
            this.donationsDataGridView.AllowUserToAddRows = false;
            this.donationsDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.donationsDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.donationsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.donationsDataGridView.Location = new System.Drawing.Point(0, 0);
            this.donationsDataGridView.Name = "donationsDataGridView";
            this.donationsDataGridView.ReadOnly = true;
            this.donationsDataGridView.RowHeadersVisible = false;
            this.donationsDataGridView.Size = new System.Drawing.Size(650, 150);
            this.donationsDataGridView.TabIndex = 0;
            this.donationsDataGridView.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.donationsDataGridView_CellMouseDown);
            // 
            // DonationsListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 283);
            this.Controls.Add(this.donationsDataGridView);
            this.Name = "DonationsListForm";
            this.Text = "DonationsListForm";
            this.Load += new System.EventHandler(this.DonationsListForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.donationsDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView donationsDataGridView;

    }
}