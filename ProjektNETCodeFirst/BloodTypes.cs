﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektNETCodeFirst
{
    public enum BloodTypes
    {
        ABRHplus,
        ABRHminus,
        ARHplus,
        ARHminus,
        BRHplus,
        BRHminus,
        ORHplus,
        ORHminus,
    }
}
