﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;


namespace ProjektNETCodeFirst
{
    public partial class EditBloodDonationForm : Form
    {
        private BloodDonation pickedBloodDonation;
        DonationsListForm donationsListForm;
        public EditBloodDonationForm(BloodDonation pickedBloodDonation, DonationsListForm donationsListForm)
        {
            InitializeComponent();
            this.pickedBloodDonation = pickedBloodDonation;
            textBox1.Text = pickedBloodDonation.ExtraInformation;
            this.donationsListForm = donationsListForm;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!textBox1.Text.Equals(pickedBloodDonation.ExtraInformation))
            {
                
                pickedBloodDonation.ExtraInformation = textBox1.Text;
                BloodBankManager.context.Entry(pickedBloodDonation).State = System.Data.Entity.EntityState.Modified;
                BloodBankManager.context.SaveChanges();
                donationsListForm.donationsDataGridView.DataSource = BloodBankManager.context.BloodDonations.Local.ToBindingList();
                MessageBox.Show("Changes saved.");
                this.Close();

            }

            else 
            {
                MessageBox.Show("No changes were made, quitting.");
                this.Close();
            }
        }

        
    }
}
