﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektNETCodeFirst
{
    public class ExampleDataFiller
    {
        public BloodBankContext bloodBankContext;
        public List<BloodDonor> ExampleDonators;
        public ExampleDataFiller (BloodBankContext bloodBankContext)
        {
            this.bloodBankContext = bloodBankContext;
            ExampleDonators=new List<BloodDonor>();


            BloodDonor bloodDonor1 = new BloodDonor()
            {
                FirstName="Marcus",
                LastName="Wilkins",
                DateofBirth=new DateTime(1960, 10, 27),
                DateOfRegistration=DateTime.Now,
                BloodType=BloodTypes.ABRHminus,


            };
            ExampleDonators.Add(bloodDonor1);

            BloodDonor bloodDonor2 = new BloodDonor()
            {
                FirstName = "Raul",
                LastName = "Davies",
                DateofBirth = new DateTime(1958, 08, 26),
                DateOfRegistration = DateTime.Now,
                BloodType = BloodTypes.ARHminus,


            };
            ExampleDonators.Add(bloodDonor2);

            BloodDonor bloodDonor3 = new BloodDonor()
            {
                FirstName = "Richard",
                LastName = "Lance",
                DateofBirth = new DateTime(1981, 07, 02),
                DateOfRegistration = DateTime.Now,
                BloodType = BloodTypes.ORHplus,


            };
            ExampleDonators.Add(bloodDonor3);

            BloodDonor bloodDonor4 = new BloodDonor()
            {
                FirstName = "Leonard",
                LastName = "Spencer",
                DateofBirth = new DateTime(1955, 06, 03),
                DateOfRegistration = DateTime.Now,
                BloodType = BloodTypes.ARHplus,


            };
            ExampleDonators.Add(bloodDonor4);

            BloodDonor bloodDonor5 = new BloodDonor()
            {
                FirstName = "Jennifer",
                LastName = "Hays",
                DateofBirth = new DateTime(1960, 01, 13),
                DateOfRegistration = DateTime.Now,
                BloodType = BloodTypes.ARHplus,


            };
            ExampleDonators.Add(bloodDonor5);

            BloodDonor bloodDonor6 = new BloodDonor()
            {
                FirstName = "Ann",
                LastName = "Spence",
                DateofBirth = new DateTime(1962, 03, 27),
                DateOfRegistration = DateTime.Now,
                BloodType = BloodTypes.ARHplus,


            };
            ExampleDonators.Add(bloodDonor6);

            BloodDonor bloodDonor7 = new BloodDonor()
            {
                FirstName = "Tasha",
                LastName = "Fisher",
                DateofBirth = new DateTime(1985, 07, 09),
                DateOfRegistration = DateTime.Now,
                BloodType = BloodTypes.ARHplus,


            };
            ExampleDonators.Add(bloodDonor7);

            BloodDonor bloodDonor8 = new BloodDonor()
            {
                FirstName = "Joseph",
                LastName = "Hunt",
                DateofBirth = new DateTime(1976, 01, 08),
                DateOfRegistration = DateTime.Now,
                BloodType = BloodTypes.ORHplus,


            };
            ExampleDonators.Add(bloodDonor8);

            BloodDonor bloodDonor9 = new BloodDonor()
            {
                FirstName = "Lester",
                LastName = "Smoot",
                DateofBirth = new DateTime(1990, 02, 25),
                DateOfRegistration = DateTime.Now,
                BloodType = BloodTypes.ABRHminus,


            };
            ExampleDonators.Add(bloodDonor9);

            BloodDonor bloodDonor10 = new BloodDonor()
            {
                FirstName = "David",
                LastName = "Fagan",
                DateofBirth = new DateTime(1993, 03, 18),
                DateOfRegistration = DateTime.Now,
                BloodType = BloodTypes.ARHplus,


            };
            ExampleDonators.Add(bloodDonor10);

            BloodDonor bloodDonor11 = new BloodDonor()
            {
                FirstName = "Michael",
                LastName = "Howell",
                DateofBirth = new DateTime(1989, 04, 12),
                DateOfRegistration = DateTime.Now,
                BloodType = BloodTypes.ARHplus,


            };
            ExampleDonators.Add(bloodDonor11);



            foreach (var donor in ExampleDonators)
            {
                bloodBankContext.BloodDonors.Add(donor);
                bloodBankContext.SaveChanges();
            }

        }


    }
}
