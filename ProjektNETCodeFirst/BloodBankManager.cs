﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using System.Data.SqlClient;


namespace ProjektNETCodeFirst
{
    public partial class BloodBankManager : Form
    {
        public static BloodBankContext context;
        public BindingSource bindingSource=new BindingSource();
        public BloodDonor pickedBloodDonor;

        public double amountofabrhplus = 0;
        public double amountofabrhminus = 0;

        public double amountofarhplus = 0;
        public double amountofarhminus = 0;

        public double amountofbrhplus = 0;
        public double amountofbrhminus = 0;

        public double amountoforhplus = 0;
        public double amountoforhminus = 0;


        
        
        public BloodBankManager()
        {
            InitializeComponent();
            context = new BloodBankContext();
            textBox1.Text = "0";
            textBox2.Text = "0";
            textBox3.Text = "0";
            textBox4.Text = "0";
            textBox5.Text = "0";
            textBox6.Text = "0";
            textBox7.Text = "0";
            textBox8.Text = "0";
           
           
            ExampleDataFiller exampleDataFiller = new ExampleDataFiller(context);
            BloodTypesTableDataFiller bloodTypesTableDataFiller = new BloodTypesTableDataFiller(context);
           /* var query = from b in context.BloodDonors
                        select b;

            foreach (var donor in query)
            {
                MessageBox.Show(donor.BloodDonorId + " " + donor.FirstName + " " + donor.LastName + " " + donor.DateofBirth.ToShortDateString() + " " + donor.BloodType);
            }*/

            dataGridView1.DataSource = context.BloodDonors.Local.ToBindingList();

            comboBox1.DataSource = Enum.GetValues(typeof(BloodTypes));
            
            
            
            


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            

        }

        private void newBloodDonorButton_Click(object sender, EventArgs e)
        {
            NewBloodDonorForm newBloodDonorForm = new NewBloodDonorForm();
            newBloodDonorForm.bloodBankManager = this;
            newBloodDonorForm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            dataGridView1.DataSource = context.BloodDonors.Local.ToBindingList();

        }

        

        private void button1_Click_1(object sender, EventArgs e)
        {
            NewBloodDonationForm newForm = new NewBloodDonationForm(this);
            newForm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DonationsListForm newForm = new DonationsListForm();
            newForm.Show();
        }

        public void updateAmounOfStoredBlood()
        {
            textBox1.Text = amountofabrhplus.ToString();
            textBox5.Text = amountofabrhminus.ToString();
            textBox2.Text = amountofarhplus.ToString();
            textBox6.Text = amountofarhminus.ToString();
            textBox3.Text = amountofbrhplus.ToString();
            textBox7.Text = amountofbrhminus.ToString();
            textBox4.Text = amountoforhplus.ToString();
            textBox8.Text = amountoforhminus.ToString();

        }

        

        private void showAllDonorsButton_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = context.BloodDonors.Local.ToBindingList();
        }

        private void filterViewByBloodType(object sender, EventArgs e)
        {
            var query = from b in context.BloodDonors
                        where (BloodTypes)comboBox1.SelectedValue == (b.BloodType)
                        select b;

            List<BloodDonor> foundMatchingDonors = new List<BloodDonor>();

            foreach (var item in query)
            {
                foundMatchingDonors.Add(item);
            }

            dataGridView1.DataSource = foundMatchingDonors;
        }

        private void dataGridView1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenu ctx = new ContextMenu();
                MenuItem listItem = new MenuItem("List of donations");
                MenuItem removalItem = new MenuItem("Remove this blood donor");
                MenuItem editItem = new MenuItem("Edit data");
                ctx.MenuItems.Add(listItem);
                ctx.MenuItems.Add(removalItem);
                ctx.MenuItems.Add(editItem);
                ctx.Show(this, dataGridView1.PointToClient(Cursor.Position));
                pickedBloodDonor = (BloodDonor)dataGridView1.CurrentRow.DataBoundItem;
                listItem.Click += new System.EventHandler(this.listItem_Click);
                removalItem.Click += new System.EventHandler(this.removalItem_Click);
                editItem.Click += new System.EventHandler(this.editItem_Click);
            }
        }

        private void listItem_Click(object sender, System.EventArgs e)
        {
            SelectedDonatorDonations newForm = new SelectedDonatorDonations(pickedBloodDonor);
            newForm.Show();

        }

        private void removalItem_Click(object sender, System.EventArgs e)
        {
            context.BloodDonors.Remove(pickedBloodDonor);
            context.SaveChanges();
            dataGridView1.DataSource = context.BloodDonors.Local.ToBindingList();
            MessageBox.Show(pickedBloodDonor.FirstName + " " + pickedBloodDonor.LastName + " removed from database.");

        }

        private void editItem_Click(object sender, System.EventArgs e)
        {
            EditDonorForm newForm = new EditDonorForm(this, pickedBloodDonor);
            newForm.Show();
        }
        

        
    }
}
