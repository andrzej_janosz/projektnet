﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;

namespace ProjektNETCodeFirst
{
    public partial class DonationsListForm : Form
    {
        private BloodDonation pickedBloodDonation;
        public DonationsListForm()
        {
            InitializeComponent();
            donationsDataGridView.DataSource = BloodBankManager.context.BloodDonations.Local.ToBindingList();
            donationsDataGridView.Columns[3].Visible = false;
            donationsDataGridView.Columns[4].Visible = false;
            
        }
        

        private void DonationsListForm_Load(object sender, EventArgs e)
        {

        }

        private void donationsDataGridView_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenu ctx = new ContextMenu();
                
                MenuItem removalItem = new MenuItem("Remove this record");
                MenuItem editItem = new MenuItem("Edit extra information");
                
                ctx.MenuItems.Add(removalItem);
                ctx.MenuItems.Add(editItem);
                ctx.Show(this, donationsDataGridView.PointToClient(Cursor.Position));
                pickedBloodDonation = (BloodDonation)donationsDataGridView.CurrentRow.DataBoundItem;
                
                removalItem.Click += new System.EventHandler(this.removalItem_Click);
                editItem.Click += new System.EventHandler(this.editItem_Click);
            }
        }

        private void editItem_Click(object sender, EventArgs e)
        {
            EditBloodDonationForm newForm = new EditBloodDonationForm(pickedBloodDonation, this);
            newForm.Show();
            
        }

        private void removalItem_Click(object sender, EventArgs e)
        {
            BloodBankManager.context.BloodDonations.Remove(pickedBloodDonation);
            BloodBankManager.context.SaveChanges();

            donationsDataGridView.DataSource = BloodBankManager.context.BloodDonations.Local.ToBindingList();
            donationsDataGridView.Columns[3].Visible = false;
            donationsDataGridView.Columns[4].Visible = false;

        }

        
    }
}
