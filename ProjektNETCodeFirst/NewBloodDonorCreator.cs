﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektNETCodeFirst
{
    public class NewBloodDonorCreator
    {
        BloodDonor newBloodDonor=new BloodDonor();
        BloodBankContext bloodBankContext;
        public NewBloodDonorCreator(BloodBankContext bloodBankContext,String firstName, String lastName, DateTime dateOfBirth, DateTime dateOfRegistration, BloodTypes bloodType)
        {
            this.bloodBankContext=bloodBankContext;
            newBloodDonor.FirstName = firstName;
            newBloodDonor.LastName = lastName;
            newBloodDonor.DateofBirth = dateOfBirth;
            newBloodDonor.DateOfRegistration = dateOfRegistration;
            newBloodDonor.BloodType = bloodType;

            bloodBankContext.BloodDonors.Add(newBloodDonor);
            bloodBankContext.SaveChanges();

            
        }
    }
}
