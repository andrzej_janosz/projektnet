﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;

namespace ProjektNETCodeFirst
{
    public partial class NewBloodDonorForm : Form
    {
        String NewDonorFirstName;
        String NewDonorLastName;
        DateTime NewDonorDateOfBirth;
        DateTime NewDonorDateofRegistration;
        BloodTypes NewDonorBloodType;
        

        public BloodBankManager bloodBankManager;

        

        
        public NewBloodDonorForm()
        {
            InitializeComponent();
        }
        public NewBloodDonorForm(BloodBankManager bloodBankManager)
        {
            this.bloodBankManager = bloodBankManager;
        }
        
       

        private void NewBloodDonorForm_Load(object sender, EventArgs e)
        {
            bloodTypecomboBox.DataSource = Enum.GetValues(typeof(BloodTypes));
        }

        private void addDonorButton_Click(object sender, EventArgs e)
        {
            NewDonorFirstName = firstNametextBox.Text;
            NewDonorLastName = lastNametextBox.Text;
            NewDonorDateOfBirth = new DateTime(dateOfBirthmonthCalendar.SelectionEnd.Year, dateOfBirthmonthCalendar.SelectionEnd.Month, dateOfBirthmonthCalendar.SelectionEnd.Day);
            //NewDonorDateOfBirth = dateOfBirthmonthCalendar.SelectionEnd;
            NewDonorDateofRegistration = DateTime.Now;
            NewDonorBloodType = (BloodTypes)bloodTypecomboBox.SelectedValue;

            NewBloodDonorCreator newBloodDonorCreator = new NewBloodDonorCreator(BloodBankManager.context, NewDonorFirstName, NewDonorLastName, NewDonorDateOfBirth, NewDonorDateofRegistration, NewDonorBloodType);
            MessageBox.Show(NewDonorFirstName + " " + NewDonorLastName + " succesfully added.");
            
            bloodBankManager.dataGridView1.DataSource = BloodBankManager.context.BloodDonors.Local.ToBindingList();
            this.Close();
            
        }

        private void bloodTypecomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
