﻿namespace ProjektNETCodeFirst
{
    partial class NewBloodDonorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.firstNametextBox = new System.Windows.Forms.TextBox();
            this.lastNametextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dateOfBirthmonthCalendar = new System.Windows.Forms.MonthCalendar();
            this.label3 = new System.Windows.Forms.Label();
            this.bloodTypecomboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.addDonorButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // firstNametextBox
            // 
            this.firstNametextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.firstNametextBox.Location = new System.Drawing.Point(0, 12);
            this.firstNametextBox.Name = "firstNametextBox";
            this.firstNametextBox.Size = new System.Drawing.Size(195, 26);
            this.firstNametextBox.TabIndex = 0;
            // 
            // lastNametextBox
            // 
            this.lastNametextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lastNametextBox.Location = new System.Drawing.Point(0, 55);
            this.lastNametextBox.Name = "lastNametextBox";
            this.lastNametextBox.Size = new System.Drawing.Size(195, 26);
            this.lastNametextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(201, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "First Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(201, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Last Name";
            // 
            // dateOfBirthmonthCalendar
            // 
            this.dateOfBirthmonthCalendar.Location = new System.Drawing.Point(281, 114);
            this.dateOfBirthmonthCalendar.MaxSelectionCount = 1;
            this.dateOfBirthmonthCalendar.Name = "dateOfBirthmonthCalendar";
            this.dateOfBirthmonthCalendar.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(318, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Date of Birth";
            // 
            // bloodTypecomboBox
            // 
            this.bloodTypecomboBox.FormattingEnabled = true;
            this.bloodTypecomboBox.Location = new System.Drawing.Point(0, 90);
            this.bloodTypecomboBox.Name = "bloodTypecomboBox";
            this.bloodTypecomboBox.Size = new System.Drawing.Size(121, 21);
            this.bloodTypecomboBox.TabIndex = 8;
            this.bloodTypecomboBox.SelectedIndexChanged += new System.EventHandler(this.bloodTypecomboBox_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(201, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Blood Type";
            // 
            // addDonorButton
            // 
            this.addDonorButton.AutoSize = true;
            this.addDonorButton.Location = new System.Drawing.Point(91, 183);
            this.addDonorButton.Name = "addDonorButton";
            this.addDonorButton.Size = new System.Drawing.Size(93, 23);
            this.addDonorButton.TabIndex = 10;
            this.addDonorButton.Text = "Add New Donor";
            this.addDonorButton.UseVisualStyleBackColor = true;
            this.addDonorButton.Click += new System.EventHandler(this.addDonorButton_Click);
            // 
            // NewBloodDonorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 294);
            this.Controls.Add(this.addDonorButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.bloodTypecomboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dateOfBirthmonthCalendar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lastNametextBox);
            this.Controls.Add(this.firstNametextBox);
            this.Name = "NewBloodDonorForm";
            this.Text = "NewBloodDonorForm";
            this.Load += new System.EventHandler(this.NewBloodDonorForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox firstNametextBox;
        public System.Windows.Forms.TextBox lastNametextBox;
        public System.Windows.Forms.MonthCalendar dateOfBirthmonthCalendar;
        public System.Windows.Forms.ComboBox bloodTypecomboBox;
        public System.Windows.Forms.Button addDonorButton;

    }
}