﻿namespace ProjektNETCodeFirst
{
    partial class SelectedDonatorDonations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.donationDataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.donationDataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // donationDataGridView1
            // 
            this.donationDataGridView1.AllowUserToAddRows = false;
            this.donationDataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.donationDataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.donationDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.donationDataGridView1.Location = new System.Drawing.Point(1, 1);
            this.donationDataGridView1.Name = "donationDataGridView1";
            this.donationDataGridView1.ReadOnly = true;
            this.donationDataGridView1.RowHeadersVisible = false;
            this.donationDataGridView1.Size = new System.Drawing.Size(507, 215);
            this.donationDataGridView1.TabIndex = 3;
            // 
            // SelectedDonatorDonations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 262);
            this.Controls.Add(this.donationDataGridView1);
            this.Name = "SelectedDonatorDonations";
            this.Text = "SelectedDonatorDonations";
            ((System.ComponentModel.ISupportInitialize)(this.donationDataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView donationDataGridView1;


    }
}