﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;

namespace ProjektNETCodeFirst
{
    public partial class SelectedDonatorDonations : Form
    {
        public BloodDonor pickedBloodDonor;
        public SelectedDonatorDonations(BloodDonor bloodDonor)
        {
            InitializeComponent();
            pickedBloodDonor = bloodDonor;
            var query = from b in BloodBankManager.context.BloodDonations
                        where pickedBloodDonor.LastName.Equals(b.LastName)
                        select b;

            List<BloodDonation> foundMatchingDonations = new List<BloodDonation>();

            foreach (var item in query)
            {
                foundMatchingDonations.Add(item);
            }

            donationDataGridView1.DataSource = foundMatchingDonations;
            donationDataGridView1.Columns[3].Visible = false;
            donationDataGridView1.Columns[4].Visible = false;
        }
    }
}
