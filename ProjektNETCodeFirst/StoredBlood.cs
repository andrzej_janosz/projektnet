﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektNETCodeFirst
{
    public class StoredBlood
    {
        public StoredBlood()
        {
            this.AmountInStorage +=0.45;
        }
        public int StoredBloodId { get; set; }
        public BloodTypes bloodType { get; set; }
        public double AmountInStorage { get; set; }

        public virtual List<BloodDonation> bloodDonationsOfThisBloodType { get; set; }
    }
}
