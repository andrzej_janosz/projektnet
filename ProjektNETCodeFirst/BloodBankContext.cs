﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ProjektNETCodeFirst
{
    public class BloodBankContext: DbContext
    {
        public BloodBankContext() : base("BloodBankDatabase1") 
        {
            Database.SetInitializer<BloodBankContext>(new DropCreateDatabaseAlways<BloodBankContext>());
        }
        public DbSet<BloodDonor> BloodDonors { get; set; }
        public DbSet<BloodDonation> BloodDonations { get; set; }
        public DbSet<StoredBlood> storedBlood { get; set; }    
    }
}
