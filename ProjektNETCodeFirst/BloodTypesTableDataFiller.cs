﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektNETCodeFirst
{
    public class BloodTypesTableDataFiller
    {
        public BloodBankContext bloodBankContext;
        public List<StoredBlood> storedBlood;

        public BloodTypesTableDataFiller(BloodBankContext bloodBankContext)
        {
            this.bloodBankContext = bloodBankContext;
            storedBlood=new List<StoredBlood>();

            StoredBlood storedBlood1 = new StoredBlood()
            {
                bloodType = BloodTypes.ABRHplus,
                AmountInStorage = 0,

            };
            storedBlood.Add(storedBlood1);

            StoredBlood storedBlood2 = new StoredBlood()
            {
                bloodType = BloodTypes.ABRHminus,
                AmountInStorage = 0,

            };
            storedBlood.Add(storedBlood2);

            StoredBlood storedBlood3 = new StoredBlood()
            {
                bloodType = BloodTypes.ARHplus,
                AmountInStorage = 0,

            };
            storedBlood.Add(storedBlood3);

            StoredBlood storedBlood4 = new StoredBlood()
            {
                bloodType = BloodTypes.ARHminus,
                AmountInStorage = 0,

            };
            storedBlood.Add(storedBlood4);

            StoredBlood storedBlood5 = new StoredBlood()
            {
                bloodType = BloodTypes.BRHplus,
                AmountInStorage = 0,

            };
            storedBlood.Add(storedBlood5);

            StoredBlood storedBlood6 = new StoredBlood()
            {
                bloodType = BloodTypes.BRHminus,
                AmountInStorage = 0,

            };
            storedBlood.Add(storedBlood6);

            StoredBlood storedBlood7 = new StoredBlood()
            {
                bloodType = BloodTypes.ORHplus,
                AmountInStorage = 0,

            };
            storedBlood.Add(storedBlood7);

            StoredBlood storedBlood8 = new StoredBlood()
            {
                bloodType = BloodTypes.ORHminus,
                AmountInStorage = 0,

            };
            storedBlood.Add(storedBlood8);

            foreach (var bloodtype in storedBlood)
            {
                bloodBankContext.storedBlood.Add(bloodtype);
                bloodBankContext.SaveChanges();
            } 
        }
    }
}
